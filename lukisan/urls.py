from django.contrib import admin
from django.urls import *
from .views import *

app_name='lukisan'
urlpatterns = [
    path('', index, name='index'),
    path('login_page/', login_page, name='login_page'),
    path('pengguna/penyewa/', penyewa, name='penyewa'),
    path('pengguna/pemilik/', pemilik, name='pemilik'),
    path('pengguna/staf/', staf, name='staf'),
    path('page_lukisan/', page_lukisan, name='page_lukisan'),
    path('transaksi/sewa', sewa, name='sewa'),
    path('transaksi/fee', fee_func, name='fee'),
    path('transaksi/mitra', mitra, name='mitra'),
]