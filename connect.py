import psycopg2

DB_NAME = "qbnhvcwi"
DB_USER = "qbnhvcwi"
DB_PASS = "NBGODrUBuhITqs7h6fZ6T-6T_vw-nprT"
DB_HOST = "rajje.db.elephantsql.com"
DB_PORT = "5432"

conn = psycopg2.connect(database = DB_NAME, user = DB_USER, password = DB_PASS, host = DB_HOST, port = DB_PORT)

cur  = conn.cursor()

##cur.execute("""
##drop schema penyewaanlukisan cascade
##""")

cur.execute("""
create schema penyewaanlukisan
""")

cur.execute("""
set search_path to penyewaanlukisan
""")

cur.execute("""
create table pelanggan (
Id_pelanggan Varchar(10) NOT NULL,
No_ktp Varchar(20) NOT NULL,
Nama Varchar(255) NOT NULL,
Alamat Text NOT NULL,
Ttl date NOT NULL,
No_hp Varchar(15) NOT NULL,
Email varchar(255) NOT NULL,
PRIMARY KEY (id_pelanggan)
)
""")

cur.execute("""
create table penyewa (
Id_pelanggan Varchar(10) NOT NULL,
Pekerjaan Varchar(255) NOT NULL,
Kategori Varchar(10) NOT NULL,
PRIMARY KEY (id_pelanggan),
FOREIGN KEY (id_pelanggan) REFERENCES pelanggan(id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table pemilik (
Id_pelanggan Varchar(10) NOT NULL,
No_rek Varchar(25) NOT NULL,
Nama_Bank Varchar(255) NOT NULL,
PRIMARY KEY (id_pelanggan),
FOREIGN KEY (id_pelanggan) REFERENCES pelanggan(id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table staff (
No_staff Varchar(10) NOT NULL,
Nama Integer NOT NULL,
Ttl date NOT NULL,
No_hp Varchar(15) NOT NULL,
Email varchar(255) NOT NULL,
PRIMARY KEY (no_staff)
)
""")

cur.execute("""
create table lukisan (
Id_lukisan Varchar(10) NOT NULL,
Nama_lukisan Varchar(255) NOT NULL,
Tema_lukisan Text NOT NULL,
Harga integer ,
Tahun Varchar(5) NOT NULL,
Id_Pelanggan Varchar(10) NOT NULL,
PRIMARY KEY (id_lukisan),
FOREIGN KEY (id_pelanggan) REFERENCES pemilik(id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table mitra_bank (
Kode_bank Varchar(5) NOT NULL,
Nama_bank Varchar(255) NOT NULL,
PRIMARY KEY (Kode_bank)
)
""")

cur.execute("""
create table tenor_bayar (
Kode_tenor Varchar(10) NOT NULL,
Tenor_pilihan Varchar(255) NOT NULL,
Kode_bank Varchar(5) NOT NULL,
PRIMARY KEY (Kode_tenor),
FOREIGN KEY (Kode_bank) REFERENCES mitra_bank(kode_bank) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table transaksi_sewa (
Tgl_sewa date NOT NULL,
Tgl_kembali date NOT NULL,
Banyaknya Integer NOT NULL,
Lama_sewa Integer NOT NULL,
No_staff Varchar(10) NOT NULL,
Id_Pelanggan Varchar(10) NOT NULL,
Id_lukisan Varchar(10) NOT NULL,
PRIMARY KEY (tgl_sewa, id_pelanggan),
FOREIGN KEY (no_staff) REFERENCES staff(no_staff) ON
UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_pelanggan) REFERENCES penyewa(id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_lukisan) REFERENCES lukisan(id_lukisan) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table biaya_sewa (
Tgl_sewa date NOT NULL,
Biaya_sewa_lukisan Integer NOT NULL,
Alamat_tujuan Text NOT NULL,
Denda Integer NOT NULL,
Ongkir Integer NOT NULL,
Total_biaya Integer NOT NULL,
Tgl_bayar_sewa Date NOT NULL,
Tgl_jatuh_tempo Date NOT NULL,
Kode_tenor Varchar(10) NOT NULL,
Id_Pelanggan Varchar(10) NOT NULL,
PRIMARY KEY (tgl_sewa),
FOREIGN KEY (tgl_sewa, id_pelanggan) REFERENCES transaksi_sewa(Tgl_sewa, Id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (kode_tenor) REFERENCES tenor_bayar(kode_tenor) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table fee (
Tgl_terima date NOT NULL,
Jumlah integer NOT NULL,
Id_Pelanggan Varchar(10) NOT NULL,
PRIMARY KEY (Tgl_terima),
FOREIGN KEY (id_pelanggan) REFERENCES pelanggan(id_pelanggan) ON
UPDATE CASCADE ON DELETE CASCADE
)
""")

cur.execute("""
create table staff_lulusan (
No_staff Varchar(10) NOT NULL,
Lulusan_staff Varchar(255) NOT NULL,
PRIMARY KEY (no_staff)
)
""")

cur.execute("""
INSERT INTO pelanggan VALUES
    (1,'79.103.162.41','Sammy Crampton','Ocoyo','2000-06-02 00:00:00','510-758-6258','scrampton0@accuweather.com'),
    (2,'81.208.254.199','Evania Twyning','Cabanaconde','1997-06-17 00:00:00','509-211-3035','etwyning1@ow.ly'),
    (3,'110.62.43.86','Astra Brumhead','Karlskrona','1996-07-07 00:00:00','621-257-8954','abrumhead2@bigcartel.com'),
    (4,'52.231.18.125','Adelle Skelhorne','Lesnikovo','2000-02-02 00:00:00','284-524-1539','askelhorne3@google.com.br'),
    (5,'87.149.63.234','Hendrick Oddey','Kendung Timur','1996-09-11 00:00:00','502-472-5534','hoddey4@harvard.edu'),
    (6,'121.41.221.214','Minta Newlands','Karpinsk','1997-01-18 00:00:00','324-165-2739','mnewlands5@disqus.com'),
    (7,'20.79.6.140','Hagan Maddra','Stenungsund','2000-03-10 00:00:00','917-195-3040','hmaddra6@istockphoto.com'),
    (8,'209.191.124.121','Kat Guerry','Biting','1998-12-20 00:00:00','117-817-0426','kguerry7@washington.edu'),
    (9,'56.59.186.141','Rex Raffles','Dashtobod','1997-07-22 00:00:00','152-184-0086','rraffles8@tumblr.com'),
    (10,'92.207.254.101','Roselin Burdekin','Ujazd','1999-04-28 00:00:00','755-972-5060','rburdekin9@answers.com'),
    (11,'26.181.172.30','Elmer Carlucci','Taoyuan','1997-06-07 00:00:00','418-138-4763','ecarluccia@diigo.com'),
    (12,'184.17.240.44','Jerrie Sindall','Wysoka','1997-10-04 00:00:00','826-941-5501','jsindallb@earthlink.net'),
    (13,'182.250.248.25','Anthia Sainsberry','Sujiatuo','1996-04-19 00:00:00','976-732-5923','asainsberryc@tripadvisor.com'),
    (14,'236.165.185.3','Carolann Barajaz','Delmas','2000-07-02 00:00:00','482-892-9209','cbarajazd@tripadvisor.com'),
    (15,'79.18.206.176','Enrica Surmeyers','Cedry Wielkie','1996-01-26 00:00:00','955-269-9220','esurmeyerse@marriott.com'),
    (16,'189.193.68.237','Korry Briers','Ismailia','1995-12-20 00:00:00','826-908-8128','kbriersf@facebook.com'),
    (17,'233.110.85.241','Tam Clemson','Banfora','1999-10-20 00:00:00','441-917-1426','tclemsong@rediff.com'),
    (18,'138.240.242.69','Bernard Nickerson','Khadan Khāk','1998-05-22 00:00:00','935-754-4030','bnickersonh@nifty.com'),
    (19,'216.91.228.87','Cozmo Gonnet','Tanagara','1997-06-30 00:00:00','582-264-0073','cgonneti@rakuten.co.jp'),
    (20,'247.225.195.232','Malissia Mossbee','Gachalá','1997-04-30 00:00:00','583-379-0536','mmossbeej@ning.com'),
    (21,'131.112.148.146','Connie Bawme','Hà Đông','1996-02-23 00:00:00','345-123-2409','cbawmek@liveinternet.ru'),
    (22,'125.22.197.213','Donal Stronghill','Pekalongan','2000-02-23 00:00:00','489-886-6740','dstronghilll@dedecms.com'),
    (23,'137.66.98.206','Parrnell Waumsley','Knoxville','1999-02-03 00:00:00','865-607-5557','pwaumsleym@last.fm'),
    (24,'117.45.150.188','Alida Attride','Lodhrān','1999-05-04 00:00:00','726-893-3196','aattriden@redcross.org'),
    (25,'81.173.213.131','Dick Coghlan','Cendagah','1999-01-04 00:00:00','547-707-3562','dcoghlano@bravesites.com'),
    (26,'234.248.241.74','Haskel Merry','Somdet','1996-08-03 00:00:00','193-393-5308','hmerryp@squarespace.com'),
    (27,'235.109.173.41','Ebonee Candelin','Chengji','1999-06-19 00:00:00','214-607-3810','ecandelinq@nih.gov'),
    (28,'114.150.200.213','Shaine Mote','Kochani','1996-09-22 00:00:00','172-785-7229','smoter@ucsd.edu'),
    (29,'193.26.122.194','Luisa Snar','Nyima','1998-06-09 00:00:00','265-386-0122','lsnars@slate.com'),
    (30,'72.172.11.172','Tracy Christy','Guanzhou','1998-03-11 00:00:00','808-407-4478','tchristyt@pbs.org'),
    (31,'70.111.84.247','Torre Abramamovh','Šluknov','2000-09-03 00:00:00','411-281-5303','tabramamovhu@mozilla.org'),
    (32,'146.72.91.158','Israel Hains','Beixiaoying','2000-06-09 00:00:00','157-691-4671','ihainsv@squarespace.com'),
    (33,'29.102.216.208','Berti Antoniak','Fortaleza','1996-04-07 00:00:00','357-421-6279','bantoniakw@reverbnation.com'),
    (34,'198.223.222.203','Jaquenette Piel','Kore','1998-10-20 00:00:00','170-512-1023','jpielx@sourceforge.net'),
    (35,'59.170.30.131','Clayson McGiven','Liuliping','1997-11-06 00:00:00','598-860-3925','cmcgiveny@mayoclinic.com'),
    (36,'20.41.45.43','Eli Coldman','Guachucal','1997-05-05 00:00:00','782-241-5718','ecoldmanz@weebly.com'),
    (37,'133.241.150.133','Mariele McAuslan','Krajan Keboromo','1999-08-29 00:00:00','415-634-3747','mmcauslan10@technorati.com'),
    (38,'202.113.107.186','Edmund Boughtwood','Ovruch','1997-10-09 00:00:00','178-833-4009','eboughtwood11@weibo.com'),
    (39,'242.182.55.142','Sofie Roskelly','Chenqing','1999-07-24 00:00:00','341-811-6279','sroskelly12@nba.com'),
    (40,'44.232.108.58','Veda Calkin','Władysławowo','1998-10-21 00:00:00','825-713-9960','vcalkin13@cpanel.net'),
    (41,'79.165.23.138','Hildegarde Dukelow','Zhuping','1997-06-21 00:00:00','810-764-3343','hdukelow14@wp.com'),
    (42,'128.31.94.236','Katy Filtness','Qārah','1999-06-21 00:00:00','629-681-2137','kfiltness15@tripod.com'),
    (43,'4.32.132.173','Ardys Deners','Shkurinskaya','1996-03-05 00:00:00','467-152-7793','adeners16@cloudflare.com'),
    (44,'143.176.231.127','Sybille Hebbard','Kayakent','1999-12-17 00:00:00','575-906-1776','shebbard17@unc.edu'),
    (45,'128.215.115.210','Nikkie Fossett','Baih','1999-09-02 00:00:00','359-311-2945','nfossett18@archive.org'),
    (46,'89.38.247.146','Daria Sanney','Żabia Wola','1998-06-01 00:00:00','834-547-9540','dsanney19@ask.com'),
    (47,'215.165.254.54','Elena Beardwell','Hushan','1997-03-25 00:00:00','864-758-6447','ebeardwell1a@apache.org'),
    (48,'124.98.157.171','Lynea Setch','Fujioka','1996-10-17 00:00:00','167-294-0391','lsetch1b@cdbaby.com'),
    (49,'166.109.249.33','Rossy Waplinton','Mitrópoli','2000-04-09 00:00:00','385-748-1747','rwaplinton1c@state.tx.us'),
    (50,'13.188.158.126','Karrie Delleschi','Bun Barat','1999-04-22 00:00:00','755-193-4711','kdelleschi1d@utexas.edu')
""")

cur.execute("""
INSERT INTO penyewa VALUES
    (12,'Physical Therapy Assistant','P'),
    (32,'Developer II','S'),
    (49,'Software Engineer IV','B'),
    (37,'Physical Therapy Assistant','B'),
    (21,'Geological Engineer','G'),
    (50,'Marketing Manager','B'),
    (8,'Occupational Therapist','S'),
    (17,'Tax Accountant','G'),
    (46,'Human Resources Manager','S'),
    (34,'Teacher','B'),
    (13,'Civil Engineer','S'),
    (20,'Associate Professor','G'),
    (1,'Account Coordinator','S'),
    (3,'Paralegal','S'),
    (27,'Senior Financial Analyst','B'),
    (26,'Environmental Specialist','S'),
    (38,'Quality Engineer','B'),
    (18,'Recruiting Manager','S'),
    (45,'Community Outreach Specialist','B'),
    (16,'Business Systems Development Analyst','B'),
    (36,'Payment Adjustment Coordinator','S'),
    (19,'Internal Auditor','S'),
    (24,'Budget/Accounting Analyst III','P'),
    (4,'Marketing Assistant','S'),
    (2,'Product Engineer','S')
""")

cur.execute("""
INSERT INTO pemilik VALUES
    (22,'183-59-6322','BANK DKI'),
    (31,'357-57-4695','BRTPN'),
    (4,'655-67-2138','BANK DKI'),
    (46,'188-47-3014','BRI'),
    (10,'264-01-8516','BNI'),
    (5,'349-70-1817','MANDIRI'),
    (6,'712-04-8985','BRTPN'),
    (47,'494-29-6772','MANDIRI'),
    (32,'168-24-4101','CIMB'),
    (20,'242-91-1084','BNI'),
    (44,'497-20-4903','BANK DKI'),
    (33,'139-32-8042','BUKOPIN'),
    (45,'857-66-4393','BUKOPIN'),
    (48,'728-78-1016','BRTPN'),
    (27,'371-72-8720','BANK DKI'),
    (11,'733-24-2994','BRI'),
    (2,'600-55-3161','CIMB'),
    (40,'732-10-0568','CIMB'),
    (35,'205-87-5498','MANDIRI'),
    (41,'541-47-0782','BRI'),
    (43,'348-65-6488','BNI'),
    (23,'885-34-4896','BNI'),
    (24,'216-97-6957','BRI'),
    (49,'524-60-3537','BRI'),
    (42,'317-34-1271','BANK DKI')
""")

cur.execute("""
INSERT INTO staff VALUES
    ('S00',50001,'1991-12-10 00:00:00','796-633-9393','orentenbeck0@eventbrite.com'),
    ('S01',50002,'1992-08-31 00:00:00','366-190-8928','bblackmore1@google.pl'),
    ('S02',50003,'1995-11-03 00:00:00','431-541-6518','escandwright2@mail.ru'),
    ('S03',50004,'1994-12-25 00:00:00','337-198-0481','mchristoforou3@mayoclinic.com'),
    ('S04',50005,'1992-10-24 00:00:00','652-889-3074','kandrivot4@facebook.com'),
    ('S05',50006,'1994-06-12 00:00:00','321-562-3832','kcribbin5@guardian.co.uk'),
    ('S06',50007,'1993-05-07 00:00:00','178-473-9918','cbicheno6@sohu.com'),
    ('S07',50008,'1993-01-25 00:00:00','643-777-8384','dmallabund7@deviantart.com'),
    ('S08',50009,'1993-07-20 00:00:00','570-442-3405','kmattis8@prlog.org'),
    ('S09',500010,'1994-12-21 00:00:00','461-917-4242','iwiffill9@stanford.edu'),
    ('S10',500011,'1991-05-27 00:00:00','617-216-4649','ppillera@dot.gov'),
    ('S11',500012,'1991-11-09 00:00:00','895-167-4511','pberntssenb@com.com'),
    ('S12',500013,'1991-09-25 00:00:00','710-847-2404','bcharrettc@sciencedaily.com'),
    ('S13',500014,'1991-05-13 00:00:00','464-910-7192','jradend@trellian.com'),
    ('S14',500015,'1995-02-09 00:00:00','445-456-8866','pfawdreye@bbc.co.uk'),
    ('S15',500016,'1993-10-02 00:00:00','921-427-9112','ltavinorf@goo.ne.jp'),
    ('S16',500017,'1992-02-11 00:00:00','564-809-8400','mmcelmurrayg@wired.com'),
    ('S17',500018,'1993-08-20 00:00:00','593-501-7786','bokennyh@alibaba.com'),
    ('S18',500019,'1994-08-25 00:00:00','769-959-2709','jgowdiei@miibeian.gov.cn'),
    ('S19',500020,'1994-04-05 00:00:00','610-276-5523','osimisterj@multiply.com')
""")

cur.execute("""
INSERT INTO lukisan VALUES
    ('LK000','LUKISAN AWAN','AMARAH',500000,2011,22),
    ('LK001','LUKISAN ABSTRAK','SUKACITA',75000,2008,31),
    ('LK002','LUKISAN PANTAI','AMARAH',500000,1995,4),
    ('LK003','LUKISAN POHON','KESEDIHAN',500000,1994,46),
    ('LK004','LUKISAN POHON','KESEJUKAN',500000,1995,10),
    ('LK005','LUKISAN LANGIT','KESEDIHAN',1000000,2001,5),
    ('LK006','LUKISAN PENARI','KEGEMBIRAAN',1000000,2005,6),
    ('LK007','LUKISAN PANTAI','AMARAH',200000,2001,47),
    ('LK008','LUKISAN LANGIT','KESEJUKAN',500000,1984,32),
    ('LK009','LUKISAN AWAN','KESEJUKAN',440000,2001,20),
    ('LK010','LUKISAN SAWAH','KESEDIHAN',75000,1992,44),
    ('LK011','LUKISAN PANTAI','SUKACITA',500000,2009,33),
    ('LK012','LUKISAN AWAN','KESEDIHAN',200000,1996,45),
    ('LK013','LUKISAN AWAN','KEGEMBIRAAN',100000,2008,48),
    ('LK014','LUKISAN POHON','KESEJUKAN',440000,1992,27),
    ('LK015','LUKISAN SAWAH','KESEJUKAN',440000,2007,11),
    ('LK016','LUKISAN PENARI','AMARAH',500000,2009,2),
    ('LK017','LUKISAN POHON','AMARAH',100000,1999,40),
    ('LK018','LUKISAN AWAN','SUKACITA',100000,2006,35),
    ('LK019','LUKISAN LANGIT','KESEDIHAN',1000000,1989,41),
    ('LK020','LUKISAN PENARI','KESEDIHAN',1000000,1997,43),
    ('LK021','LUKISAN AWAN','KEGEMBIRAAN',100000,2005,23),
    ('LK022','LUKISAN SAWAH','AMARAH',440000,1999,24),
    ('LK023','LUKISAN POHON','KESEDIHAN',750000,1995,49),
    ('LK024','LUKISAN SAWAH','KESEJUKAN',75000,2012,42)
""")

cur.execute("""
INSERT INTO staff_lulusan VALUES
    ('S00','AKUNTANSI'),
    ('S01','VOKASI PERIKLANAN'),
    ('S02','ADVERTISING'),
    ('S03','AKUNTANSI'),
    ('S04','AKUNTANSI'),
    ('S05','AKUNTANSI'),
    ('S06','AKUNTANSI'),
    ('S07','AKUNTANSI'),
    ('S08','SEKOLAH TINGGI MANAJEMEN'),
    ('S09','VOKASI PERIKLANAN'),
    ('S10','AKUNTANSI'),
    ('S11','SEKOLAH BISNIS'),
    ('S12','VOKASI PERIKLANAN'),
    ('S13','VOKASI PERIKLANAN'),
    ('S14','VOKASI PERIKLANAN'),
    ('S15','AKUNTANSI'),
    ('S16','AKUNTANSI'),
    ('S17','POLITEKNIK'),
    ('S18','VOKASI PERIKLANAN'),
    ('S19','SEKOLAH BISNIS')
""")

cur.execute("""
INSERT INTO transaksi_sewa VALUES
    ('2020-07-03 00:00:00','2020-11-25 00:00:00',9,4,'S00',12,'LK002'),
    ('2020-07-04 00:00:00','2020-11-25 00:00:00',8,4,'S01',21,'LK004'),
    ('2019-01-15 00:00:00','2019-12-29 00:00:00',13,5,'S19',12,'LK014'),
    ('2020-03-02 00:00:00','2020-11-17 00:00:00',16,2,'S18',21,'LK020'),
    ('2020-01-08 00:00:00','2019-08-24 00:00:00',10,7,'S15',16,'LK015'),
    ('2019-05-22 00:00:00','2019-11-18 00:00:00',18,7,'S05',20,'LK005'),
    ('2019-12-30 00:00:00','2019-05-15 00:00:00',9,9,'S19',32,'LK014'),
    ('2020-05-27 00:00:00','2020-10-31 00:00:00',11,5,'S19',20,'LK015'),
    ('2019-03-29 00:00:00','2020-10-08 00:00:00',5,6,'S11',49,'LK013'),
    ('2020-03-10 00:00:00','2019-03-14 00:00:00',14,1,'S02',37,'LK002'),
    ('2019-03-07 00:00:00','2019-11-08 00:00:00',10,7,'S10',16,'LK014'),
    ('2019-12-13 00:00:00','2019-11-14 00:00:00',20,5,'S10',38,'LK017'),
    ('2019-10-01 00:00:00','2019-03-15 00:00:00',4,10,'S01',50,'LK000'),
    ('2019-08-04 00:00:00','2019-01-31 00:00:00',10,5,'S09',8,'LK003'),
    ('2019-11-09 00:00:00','2020-10-04 00:00:00',9,7,'S18',17,'LK013'),
    ('2019-01-19 00:00:00','2019-08-03 00:00:00',18,10,'S05',46,'LK000'),
    ('2019-04-11 00:00:00','2019-11-04 00:00:00',6,4,'S18',1,'LK010'),
    ('2019-03-20 00:00:00','2019-11-20 00:00:00',14,4,'S01',49,'LK006'),
    ('2019-12-25 00:00:00','2018-12-24 00:00:00',13,6,'S10',34,'LK016'),
    ('2019-02-27 00:00:00','2019-06-12 00:00:00',15,10,'S14',46,'LK003'),
    ('2019-08-06 00:00:00','2020-01-17 00:00:00',7,6,'S11',45,'LK008'),
    ('2019-06-14 00:00:00','2020-02-02 00:00:00',20,10,'S17',18,'LK004'),
    ('2019-09-16 00:00:00','2019-12-03 00:00:00',18,3,'S06',45,'LK013'),
    ('2019-09-13 00:00:00','2020-04-01 00:00:00',17,7,'S00',1,'LK007'),
    ('2019-09-09 00:00:00','2019-12-07 00:00:00',16,7,'S00',36,'LK017')
""")

cur.execute("""
INSERT INTO fee VALUES
    ('2019-12-02 00:00:00',8,11),
    ('2019-11-01 00:00:00',2,2),
    ('2020-11-15 00:00:00',2,5),
    ('2020-10-09 00:00:00',1,40),
    ('2020-02-26 00:00:00',3,23),
    ('2020-04-08 00:00:00',2,10),
    ('2020-10-28 00:00:00',6,16),
    ('2020-01-21 00:00:00',10,48),
    ('2020-11-10 00:00:00',6,20),
    ('2020-07-26 00:00:00',8,28),
    ('2020-06-18 00:00:00',4,48),
    ('2019-09-20 00:00:00',2,41),
    ('2020-11-08 00:00:00',9,30),
    ('2018-12-26 00:00:00',10,3),
    ('2020-07-10 00:00:00',9,26),
    ('2019-01-11 00:00:00',2,21),
    ('2020-08-22 00:00:00',8,6),
    ('2020-01-13 00:00:00',7,15),
    ('2020-01-22 00:00:00',1,32),
    ('2020-11-19 00:00:00',3,49),
    ('2019-11-03 00:00:00',9,47),
    ('2019-04-16 00:00:00',3,31),
    ('2019-07-23 00:00:00',7,22),
    ('2020-08-01 00:00:00',5,47)
""")

cur.execute("""
INSERT INTO mitra_bank VALUES
    (001,'A'),
    (002,'B'),
    (003,'C'),
    (004,'D'),
    (005,'E'),
    (006,'F'),
    (007,'G'),
    (008,'H'),
    (009,'I'),
    (010,'J')
""")

cur.execute("""
INSERT INTO tenor_bayar VALUES
    ('T01','AAAA',001),
    ('T02','BBBB',002),
    ('T03','CCCC',003),
    ('T04','DDDD',004)
""")


cur.execute("""
INSERT INTO biaya_sewa VALUES
 ('2020-07-03 00:00:00',585000,'257 International Center',33000,86000,704000,'2019-12-30 00:00:00','2019-12-28 00:00:00','T03',12),
 ('2020-07-04 00:00:00',759000,'60265 Lakewood Gardens Terrace',44000,78000,881000,'2019-09-02 00:00:00','2019-09-07 00:00:00','T01',21),
 ('2019-01-15 00:00:00',332000,'2 Continental Junction',71000,70000,473000,'2019-08-25 00:00:00','2019-08-28 00:00:00','T01',12),
 ('2020-03-02 00:00:00',371000,'4104 Becker Alley',59000,29000,459000,'2019-05-14 00:00:00','2019-05-20 00:00:00','T01',21),
 ('2020-01-08 00:00:00',544000,'3590 Loeprich Trail',67000,53000,664000,'2020-02-08 00:00:00','2020-02-06 00:00:00','T02',16),
 ('2019-05-22 00:00:00',484000,'3688 Westend Drive',79000,42000,605000,'2019-09-22 00:00:00','2019-09-22 00:00:00','T04',20),
 ('2019-12-30 00:00:00',668000,'94668 Acker Plaza',23000,96000,787000,'2020-07-27 00:00:00','2020-07-28 00:00:00','T01',32),
 ('2020-05-27 00:00:00',228000,'3 Di Loreto Hill',74000,81000,383000,'2020-08-23 00:00:00','2020-08-22 00:00:00','T04',20),
 ('2019-03-29 00:00:00',533000,'1 Aberg Parkway',70000,7000,664000,'2019-09-16 00:00:00','2019-09-19 00:00:00','T01',49),
 ('2020-03-10 00:00:00',519000,'128 Atwood Way',52000,93000,664000,'2020-06-24 00:00:00','2020-06-25 00:00:00','T02',37),
 ('2019-03-07 00:00:00',336000,'65 Crescent Oaks Circle',68000,39000,443000,'2019-08-20 00:00:00','2019-08-17 00:00:00','T02',16),
 ('2019-12-13 00:00:00',403000,'56780 Acker Park',15000,75000,493000,'2020-01-25 00:00:00','2020-01-26 00:00:00','T02',38),
 ('2019-10-01 00:00:00',552000,'3 Autumn Leaf Parkway',10000,3000,565000,'2020-01-29 00:00:00','2020-01-02 00:00:00','T02',50),
 ('2019-08-04 00:00:00',390000,'30 Ronald Regan Park',74000,32000,496000,'2019-12-02 00:00:00','2019-11-01 00:00:00','T04',8),
 ('2019-11-09 00:00:00',314000,'36 Washington Avenue',42000,86000,442000,'2020-01-29 00:00:00','2019-12-31 00:00:00','T04',17),
 ('2019-01-19 00:00:00',620000,'33556 Lakewood Gardens Park',88000,79000,787000,'2019-12-04 00:00:00','2019-12-04 00:00:00','T04',46),
 ('2019-04-11 00:00:00',550000,'213 4th Place',51000,82000,683000,'2019-07-28 00:00:00','2019-07-01 00:00:00','T04',1),
 ('2019-03-20 00:00:00',604000,'55 Daystar Street',23000,35000,662000,'2019-07-30 00:00:00','2019-07-28 00:00:00','T04',49),
 ('2019-12-25 00:00:00',188000,'72328 Morrow Pass',51000,26000,265000,'2020-05-10 00:00:00','2020-04-09 00:00:00','T02',34),
 ('2019-02-27 00:00:00',320000,'38988 Mesta Pass',42000,5000,367000,'2019-03-02 00:00:00','2019-03-07 00:00:00','T01',46),
 ('2019-08-06 00:00:00',318000,'29833 Rockefeller Pass',30000,10000,358000,'2019-12-06 00:00:00','2019-12-09 00:00:00','T04',45),
 ('2019-06-14 00:00:00',194000,'632 Ryan Way',54000,2000,250000,'2019-08-12 00:00:00','2019-08-16 00:00:00','T01',18),
 ('2019-09-16 00:00:00',713000,'0 School Pass',24000,62000,799000,'2020-01-08 00:00:00','2019-12-10 00:00:00','T02',45),
 ('2019-09-13 00:00:00',512000,'990 Pleasure Plaza',17000,99000,628000,'2019-11-20 00:00:00','2019-11-21 00:00:00','T02',1),
 ('2019-09-09 00:00:00',835000,'5277 Red Cloud Park',86000,97000,1018000,'2019-10-13 00:00:00','2019-10-13 00:00:00','T02',36)
""")

conn.commit()
